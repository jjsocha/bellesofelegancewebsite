<!DOCTYPE html>
<html lang="en">
<head>
	<link href="<?php echo base_url('/assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('/assets/css/custom.css'); ?>" rel="stylesheet">
	<title><?php if(isset($title)) echo $title; ?></title>
  <link rel="icon" type="image/x-icon" href="<?php echo base_url('assets/favicon.ico'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
</head>
<body>
<div class="container-fluid" style="max-width:1170px;">
<div class="row">
	<div class="col-lg-10 col-md-9 col-sm-8 col-xs-7">
		<h1>Belles of Elegance</h1>
	</div>
	<div class="col-lg-2 col-md-3 col-sm-4 col-xs-5">
		<p>
			<b><span class="glyphicon glyphicon-time"></span> Store Hours</b><br/>
			M-TH 10:00AM to 7:00PM<br/>
			F 10:00AM to 5:00PM<br/>
			SAT10:00AM to 4:00PM<br/>
      <i>Call for seasonal hours</i>
		</p>
	</div>
</div>
<div class="row">
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
</div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li <?php if($page=="home") echo 'class="active"';?>><a href="<?php echo base_url();?>">Home</a></li>
        <li <?php if($page=="gowns") echo 'class="active"';?>><a href="<?php echo base_url('Gowns');?>">Gowns</a></li>
        <li <?php if($page=="tuxedos") echo 'class="active"';?>><a href="<?php echo base_url('Tuxedos');?>">Tuxedos</a></li>
        <li <?php if($page=="accessories") echo 'class="active"';?>><a href="<?php echo base_url('Accessories');?>">Accessories</a></li>
        <li <?php if($page=="location") echo 'class="active"';?>><a href="<?php echo base_url('Location');?>">Location</a></li>
        <li <?php if($page=="calendar") echo 'class="active"';?>><a href="<?php echo base_url('Calendar');?>">Calendar</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
