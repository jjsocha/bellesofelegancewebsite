<div class="jumbotron">
	<h1>Welcome!</h1>
	<p>
		Welcome to Belles of Elegance, Northeast Wisconsin's largest bridal shop with over 4000 square feet of showroom. 
		With more than 500 bridals, 400 bridesmaids, and 300 special occasion gowns in stock and ready for immediate 
		purchase or special order.
	</p>
</div>
<div class="row">
	<div class="col-lg-6 col-md-6">
		<p>Come in and enjoy our large bridal fitting rooms, and have our well trained and knowledgeable staff help you select that perfect gown.</p>
		<p>After you have selected your gown allow our staff to help you put on the crowning touches. We offer many accessories to finish off your wedding attire, including a specialty line of custom jewelry designed for Belles of Elegance by a certified gemologist.</p>
		<p>Belles of Elegance has an excellent in house alterations staff with more than 26 years of experience to ensure you the perfect fit.</p>
		<p>We invite you to call and schedule your appointment with one of our bridal consultants to see our large selection of gowns.</p>
		<p><span class="glyphicon glyphicon-phone-alt"> 920-339-0899</span><p>
		<a class="btn btn-success" href="<?php echo base_url('Location');?>">Location</a>
	</div>
</div>