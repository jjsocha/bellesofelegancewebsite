<div class="page-header">
	<h2>Gowns <small><?php echo $section; ?></small></h2>
</div>
<div class="row">
	<?php $this->load->view('pages/gowns/templates/menu'); ?>
	<div class="col-lg-6 col-md-6 col-sm-9 col-xs-8">
		<p>Whether your wedding day is formal or a garden setting, we offer a large selection of choices in both full length and tea-length gowns.</p>
		<p>We invite you to call ahead to make an appointment to reserve a room for you and your bridal group to view our full collection.</p>
		<p><span class="glyphicon glyphicon-phone-alt"> 920-339-0899</span></p>
		<a class="btn btn-success" href="<?php echo base_url('Location');?>">Location</a>
	</div>
</div>