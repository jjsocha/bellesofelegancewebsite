<div class="page-header">
	<h2>Gowns <small><?php echo $section; ?></small></h2>
</div>
<div class="row">
	<?php $this->load->view('pages/gowns/templates/menu'); ?>
	<div class="col-lg-6 col-md-6 col-sm-9 col-xs-8">
		<p>We know your wedding gown is the most important part of your wedding day because it will set the tone of your wedding.</p>
		<p>We invite you to look over some of our major manufactures that we have available for your viewing.  They carry the most current styles in the bridal industry.</p>
		<p>We also invite you to call and make an appointment with our professional bridal consultants and reserve a bridal room.</p>
		<p><span class="glyphicon glyphicon-phone-alt"> 920-339-0899</span></p>
		<a class="btn btn-success" href="<?php echo base_url('Location');?>">Location</a>
	</div>
</div>