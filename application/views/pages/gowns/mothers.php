<div class="page-header">
	<h2>Gowns <small><?php echo $section; ?></small></h2>
</div>
<div class="row">
	<?php $this->load->view('pages/gowns/templates/menu'); ?>
	<div class="col-lg-6 col-md-6 col-sm-9 col-xs-8">
		<p>For an elegant look for the mother of the bride or groom, we carry special occasion gowns in a variety of styles and colors from several major designers. Please check the following sites:</p>
		<p>
			<button type="button" class="btn btn-info">AMAC</button>
			<button type="button" class="btn btn-info">Avanti</button>
			<button type="button" class="btn btn-info">LaBelle</button>
			<button type="button" class="btn btn-info">Landa Designs</button>
		</p>
		<p>We invite you to call and schedule your appointment with one of our bridal consultants today.</p>
		<p><span class="glyphicon glyphicon-phone-alt"> 920-339-0899</span></p>
		<a class="btn btn-success" href="<?php echo base_url('Location');?>">Location</a>
	</div>
</div>