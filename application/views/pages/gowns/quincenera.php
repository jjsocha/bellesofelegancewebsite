<div class="page-header">
	<h2>Gowns <small><?php echo $section; ?></small></h2>
</div>
<div class="row">
	<?php $this->load->view('pages/gowns/templates/menu'); ?>
	<div class="col-lg-6 col-md-6 col-sm-9 col-xs-8">
		<p>Fully beaded quincea&ntilde;era ball gowns will make you feel so beautiful on your special day. Browse through the following websites to see the collections of qui&ntilde;ceanera gowns we offer.</p>
		<p>
			<button type="button" class="btn btn-info">Mori Lee</button>
			<button type="button" class="btn btn-info">P.C. Mary's</button>
		</p>
		<p>We invite you to call ahead and schedule your appointment to come in and visit our store.</p>
		<p><span class="glyphicon glyphicon-phone-alt"> 920-339-0899</span></p>
		<a class="btn btn-success" href="<?php echo base_url('Location');?>">Location</a>
	</div>
</div>