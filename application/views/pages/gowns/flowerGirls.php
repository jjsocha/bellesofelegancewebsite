<div class="page-header">
	<h2>Gowns <small><?php echo $section; ?></small></h2>
</div>
<div class="row">
	<?php $this->load->view('pages/gowns/templates/menu'); ?>
	<div class="col-lg-6 col-md-6 col-sm-9 col-xs-8">
		<p>For the smallest member of your wedding party, we carry an array of flower girl gowns to coordinate with the bridal gowns available today. Just click on the following websites to see some of the many options available to today's bride.</p>
		<p>
			<button type="button" class="btn btn-info">Mori Lee</button>
			<button type="button" class="btn btn-info">Forever Yours</button>
			<button type="button" class="btn btn-info">Venus</button>
			<button type="button" class="btn btn-info">Tiffany</button>
		</p>
		<p>We invite you to call and schedule your appointment and come in and look at our large selection of flowergirls dresses in store today.</p>
		<p><span class="glyphicon glyphicon-phone-alt"> 920-339-0899</span></p>
		<a class="btn btn-success" href="<?php echo base_url('Location');?>">Location</a>
	</div>
</div>