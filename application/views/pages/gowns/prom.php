<div class="page-header">
	<h2>Gowns <small><?php echo $section; ?></small></h2>
</div>
<div class="row">
	<?php $this->load->view('pages/gowns/templates/menu'); ?>
	<div class="col-lg-6 col-md-6 col-sm-9 col-xs-8">
		<p>Please call and set-up your appointment for a viewing of all our special occasion gowns.</p>
		<p><span class="glyphicon glyphicon-phone-alt"> 920-339-0899</span></p>
		<a class="btn btn-success" href="<?php echo base_url('Location');?>">Location</a>
	</div>
</div>