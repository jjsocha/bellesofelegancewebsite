<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
	<ul class="nav nav-pills nav-stacked">
		<li <?php if($section == 'Bridal') echo 'class="active"';?>><a href="<?php echo base_url("Gowns/Bridal"); ?>">Bridal</a></li>
		<li <?php if($section == 'Bridesmaids') echo 'class="active"';?>><a href="<?php echo base_url("Gowns/Bridesmaids"); ?>">Bridesmaids</a></li>
		<li <?php if($section == 'Mothers') echo 'class="active"';?>><a href="<?php echo base_url("Gowns/Mothers"); ?>">Mothers</a></li>
		<li <?php if($section == 'Prom') echo 'class="active"';?>><a href="<?php echo base_url("Gowns/Prom"); ?>">Prom</a></li>
		<li <?php if($section == 'Flower Girls') echo 'class="active"';?>><a href="<?php echo base_url("Gowns/FlowerGirls"); ?>">Flower Girls</a></li>
		<li <?php if($section == 'Quince&ntilde;era') echo 'class="active"';?>><a href="<?php echo base_url("Gowns/Quincenera"); ?>">Quince&ntilde;era</a></li>
	</ul>
</div>