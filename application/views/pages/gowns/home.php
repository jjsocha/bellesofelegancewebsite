<div class="page-header">
		<h2>Gowns <small><?php echo $section; ?></small></h2>
</div>
<div class="row">
	<?php $this->load->view('pages/gowns/templates/menu'); ?>
	<div class="col-lg-6 col-md-6 col-sm-9 col-xs-8">
		<p>
			Belles of Elegance offers one of the largest selection of bridal, bridesmaid and special occasion gowns in the Northeastern Wisconsin area.
			We carry many major bridal manufacturers including Private Label by G, Mori Lee, Forever Yours, Jacquelin and Venus. Our bridesmaid line includes gowns by Mori Lee, Alexia, Private Collection, Bella, Eden, Belsoie and B2. 
		</p>
		<p>
			Belles of Elegance also offers prom, quinceanera, mother of the bride or groom and special occasion gowns.  Our prom line includes the latest fashion from the hottest lines on the market today including Tiffany, Precious Formals, Night Time and Mori Lee. For the mother of the bride or groom, we carry fashions from Bella, Private Occasions, Karen Miller, Onyx, La Belle, Amac and Avanti.
		</p>
		<p>
			Please check out our many offerings by selecting the category your are interested in from the links to the left.
		</p>
	</div>
</div>