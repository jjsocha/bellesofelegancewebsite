<div class="page-header">
	<h2>Tuxedos</h2>
</div>
<div class="row">
	<div class="col-lg-12">
		<p>As a local retailer for one of the largest leading tuxedo companies in the industry, we offer some of the most up-to-date styles.  We carry major designer like Ralph Lauren, Calvin Klein, Jean Yves and After Six. To see our full collection, please check out their site.</p>
		<p><button type="button" class="btn btn-info">Jim's Formal Wear</button>
	</div>
</div>