<div class="page-header">
	<h2>Location</h2>
</div>
<div class="row">
	<div class="col-lg-4">
		<p>We are located at:</p>
		<p><span class="glyphicon glyphicon-map-marker"></span>112 South Broadway De Pere, WI 54115</p>
		<p>
			<b><span class="glyphicon glyphicon-time"></span> Store Hours</b><br/>
			Monday thru Thursday 10:00AM to 7:00PM<br/>
			Friday 10:00AM to 5:00PM<br/>
			Saturday 10:00AM to 4:00PM<br/>
		</p>
	</div>
	<div class="col-lg-8">
		<iframe width="95%" height="800" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=112+S+Broadway+de+pere+wi&amp;aq=t&amp;sll=47.11113,-88.561672&amp;sspn=0.050645,0.111494&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=112+S+Broadway,+De+Pere,+Wisconsin+54115&amp;z=14&amp;ll=44.448521,-88.060481&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=112+S+Broadway+de+pere+wi&amp;aq=t&amp;sll=47.11113,-88.561672&amp;sspn=0.050645,0.111494&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=112+S+Broadway,+De+Pere,+Wisconsin+54115&amp;z=14&amp;ll=44.448521,-88.060481" style="color:#0000FF;text-align:left">View Larger Map</a></small>
	</div>
</div>
