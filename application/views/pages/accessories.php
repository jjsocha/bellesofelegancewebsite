<div class="page-header">
	<h2>Accessories</h2>
</div>
<div class="row">
	<div class="col-lg-12">
		<p>
			Belles of Elegance offers a large selection of accessories to complete your entire look from head to toe. 
			Once you have chosen your gown to make the look complete, there are a few more details that you need to
			pay attention to. We carry a large selection of headpieces and veils.  We offer many options to please the
			most discriminating bride. Whether you are looking for an heirloom custom made headpiece or a very refined
			and simple piece, we carry many options for you to choose from. We also offer many options to choose from
			in vials, from the most simply cut edge veil, fully beaded edge veils and custom made veils. 
		</p>
		<p>
			At Belles of Elegance you will find the most exquisite line of designer jewelry designed by a certified gemologist. Her
			creations are fashionable and up-to-date to complement today's wedding gowns.  The construction is far more
			superb than that of any costume jewelry available on the open market today. Including the use of cubic 
			zirconium gemstones, rhodium based construction and the finishing touches of a jeweled clasp when the design
			allows.
		</p>
		<p>
			Belles of Elegance also carries one of the largest selection of formal footwear in the area.  We carry the
			most popular brands on the market today.  Please check out the following websites for a look at the styles
			that are available to you.
		</p>
		<p>
			<button type="button" class="btn btn-info">Touch Ups</button>
			<button type="button" class="btn btn-info">Coloriffics</button>
			<button type="button" class="btn btn-info">Dyeables</button>
			<button type="button" class="btn btn-info">Special Occasions</button>
		</p>
		<p>
			For that secret under the wedding gown look, come and take a peek at our lingerie that will give you that
			slender look you are after.  Longline and strapless bustiers come in a wide range of sizing.  They are
			available from 32 A up to 48DD.  Then, you may choose to finish that look off with a crinoline that gives
			your bridal gown that just the right look.  Please come in and try on your gown with these items.  One of
			our staff members will be more than able to help you with these purchases.
		</p>
		<p>
			We also carry a complete line of wedding invitations. Come in and browse through the invitation books 
			available for your viewing or check out a book out and sit in the comfort of your own home and select invitations.
		</p>
		<p><span class="glyphicon glyphicon-phone-alt"> 920-339-0899</span></p>
		<a class="btn btn-success" href="<?php echo base_url('Location');?>">Location</a>
	</div>
</div>


