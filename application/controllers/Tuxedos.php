<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tuxedos extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->data['page'] = "tuxedos";
   	}

	public function index()
	{
		$this->data['title'] = "Belles of Elegance - Tuxedos";
		$this->render_page('pages/tuxedos');
	}
	
}