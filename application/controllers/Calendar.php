<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->data['page'] = "calendar";
   	}

	public function index()
	{
		$this->data['title'] = "Belles of Elegance - Calendar";
		$this->render_page('pages/calendar');
	}
	
}