<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gowns extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->data['page'] = "gowns";
        $this->data['section'] = "";
   	}

	public function index()
	{
		$this->data['title'] = "Belles of Elegance - Gowns";
		$this->render_page('pages/gowns/home');
	}

	public function bridal()
	{
		$this->data['title'] = "Belles of Elegance - Bridal";
		$this->data['section'] = "Bridal";
		$this->render_page('pages/gowns/bridal');
	}

	public function bridesmaids()
	{
		$this->data['title'] = "Belles of Elegance - Bridesmaids";
		$this->data['section'] = "Bridesmaids";
		$this->render_page('pages/gowns/bridesmaids');
	}

	public function mothers()
	{
		$this->data['title'] = "Belles of Elegance - Mothers";
		$this->data['section'] = "Mothers";
		$this->render_page('pages/gowns/mothers');
	}

	public function prom()
	{
		$this->data['title'] = "Belles of Elegance - Prom";
		$this->data['section'] = "Prom";
		$this->render_page('pages/gowns/prom');
	}

	public function flowerGirls()
	{
		$this->data['title'] = "Belles of Elegance - Flower Girls";
		$this->data['section'] = "Flower Girls";
		$this->render_page('pages/gowns/flowerGirls');
	}

	public function quincenera()
	{
		$this->data['title'] = "Belles of Elegance - Quince&ntilde;era";
		$this->data['section'] = "Quince&ntilde;era";
		$this->render_page('pages/gowns/quincenera');
	}
	
}