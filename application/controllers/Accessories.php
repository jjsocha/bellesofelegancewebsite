<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accessories extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->data['page'] = "accessories";
   	}

	public function index()
	{
		$this->data['title'] = "Belles of Elegance - Accessories";
		$this->render_page('pages/accessories');
	}
	
}