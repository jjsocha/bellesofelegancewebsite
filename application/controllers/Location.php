<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->data['page'] = "location";
   	}

	public function index()
	{
		$this->data['title'] = "Belles of Elegance - Location";
		$this->render_page('pages/location');
	}
	
}