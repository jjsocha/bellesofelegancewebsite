<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->data['page'] = "home";
   	}

	public function index()
	{
		$this->data['title'] = "Belles of Elegance";
		$this->render_page('pages/home');
	}
	
}