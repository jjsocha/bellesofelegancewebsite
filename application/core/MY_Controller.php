<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller {

  protected $data = array();

  function __construct() {
    parent::__construct();
    $this->load->helper('url');
  }

  function render_page($view) {
    $this->load->view('templates/header', $this->data);
    $this->load->view($view, $this->data);
    $this->load->view('templates/footer', $this->data);
  }

}
?>